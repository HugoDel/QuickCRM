from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include

from QuickCRM import settings

urlpatterns = [
    path('profile/', include('qcauth.urls', namespace='qcauth')),
    path('admin/', admin.site.urls),
    path('document/', include('document.urls', namespace='doc')),
    path('projects/', include('project.urls', namespace='pj')),
    path('', include('crm.urls', namespace='crm')),
] + static(settings.STATIC_URL, document_root=settings.MEDIA_ROOT)
