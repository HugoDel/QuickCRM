from django.http import Http404
from django.shortcuts import redirect
from django.urls import reverse, reverse_lazy

from common.default_views import DefaultListView, DefaultCreateView, DefaultTabListView, DefaultDetailsView, \
    DefaultDeleteView, DefaultUpdateView
from document.models import Document
from project.models import Project
from django.utils.translation import ugettext_lazy as _


# Project view

class ProjectList(DefaultTabListView):
    model = Project
    title = _('Projets')
    nav_item = [
        (_('Non commencé'), reverse_lazy('pj:project_list_filter', kwargs={'status': 'notstart'})),
        (_('En cours'), reverse_lazy('pj:project_list_filter', kwargs={'status': 'started'})),
        (_('Fini'), reverse_lazy('pj:project_list_filter', kwargs={'status': 'finish'})),
        (_('Perdu'), reverse_lazy('pj:project_list_filter', kwargs={'status': 'lost'})),
    ]


project_list = ProjectList.as_view()


PJ_STATUS_URL_LOOKUP = {
    'notstart': Project.PJ_NOT_RUNNING,
    'started': [Project.PJ_WON],
    'finish': [Project.PJ_FINISH],
    'lost': [Project.PJ_LOST],
}


class ProjectFilterList(DefaultListView):
    model = Project
    template_name = 'project_list.jinja'

    def get_queryset(self):
        status = self.kwargs.get('status')
        if not status:
            redirect(reverse('pj:project_list'))
        status = PJ_STATUS_URL_LOOKUP.get(status)
        if not status:
            raise Http404(_('Ce status pour un projet n\'existe pas'))
        return Project.objects.filter(status__in=status)


project_list_filter = ProjectFilterList.as_view()


class AddProject(DefaultCreateView):
    model = Project
    title = _('Ajouter un Projet')
    fields = ['name', 'company', 'description', 'start_date', 'end_date', 'status', 'budget']

    def get_success_url(self):
        return reverse('pj:project_detail', kwargs={'pk': self.object.pk})


add_project = AddProject.as_view()


class ProjectDetail(DefaultDetailsView):
    model = Project
    template_name = 'project_details.jinja'
    title = _('Détails Projets')

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx.update({
            'modal_title': _('Confirmation'),
            'modal_content': _('Etes-vous sûr de supprimer cette élément'),
            'modal_redirect_url': reverse('pj:delete_project', kwargs={'pk': self.object.pk}),
            'type': Document.DOC_TYPE,
        })
        return ctx


project_detail = ProjectDetail.as_view()


class DeleteProject(DefaultDeleteView):
    model = Project
    success_url = reverse_lazy('pj:project_list')


delete_project = DeleteProject.as_view()


class UpdateProject(DefaultUpdateView):
    model = Project
    fields = AddProject.fields
    title = _('Modifier un Projet')

    def get_success_url(self):
        return reverse('pj:project_detail', kwargs={'pk': self.object.pk})


update_project = UpdateProject.as_view()
