from django.urls import path

from . import views

app_name = 'pj'
urlpatterns = [
    path('', views.project_list, name='project_list'),
    path('add/', views.add_project, name='add_project'),
    path('list/<slug:status>/', views.project_list_filter, name='project_list_filter'),
    path('<int:pk>/', views.project_detail, name='project_detail'),
    path('<int:pk>/delete/', views.delete_project, name='delete_project'),
    path('<int:pk>/update/', views.update_project, name='update_project'),
]
