from django.db import models
from django.utils.translation import ugettext_lazy as _


class Project(models.Model):

    PJ_PROSPECTION = 20
    PJ_QUALIFICATION = 40
    PJ_PROPOSAL = 60
    PJ_NEGOCIATION = 80
    PJ_WON = 100
    PJ_LOST = 0
    PJ_FINISH = 110

    PJ_STATUS = (
        (PJ_PROSPECTION, _('Prospection')),
        (PJ_QUALIFICATION, _('Qualification')),
        (PJ_PROPOSAL, _('Proposition')),
        (PJ_NEGOCIATION, _('Negociation')),
        (PJ_WON, _('Gagné')),
        (PJ_LOST, _('Perdu')),
        (PJ_FINISH, _('Finish'))
    )

    PJ_NOT_RUNNING = (PJ_PROSPECTION, PJ_QUALIFICATION, PJ_PROPOSAL, PJ_NEGOCIATION)

    _prefix = 'PJ'
    name = models.CharField(_('Nom du projet'), max_length=150)
    company = models.ForeignKey('crm.Company', related_name='projects', verbose_name=_('Entreprise'),
                                on_delete=models.DO_NOTHING)
    description = models.TextField(_('Description'), null=True, blank=True)
    date_created = models.DateTimeField(_('Date d\'ajout'), auto_now_add=True)
    last_updated = models.DateTimeField(_('Dernière modification'), auto_now=True)
    start_date = models.DateField(_('Début'), null=True, blank=True)
    end_date = models.DateField(_('Fin'), null=True, blank=True)
    status = models.SmallIntegerField(_('Status'), choices=PJ_STATUS, default=PJ_PROSPECTION)
    budget = models.IntegerField(_('Budget'), default=0, blank=True)
    deleted = models.BooleanField(_('Supprimé'), default=False)

    def ref_project_formatter(self):
        return f'{self.date_created.year}_{self.pk}_{self.company.name[:5].upper()}'

    def get_status_string(self):
        for num, string in self.PJ_STATUS:
            if num == self.status:
                return string
        return self.status

    def display_id(self):
        return f'{self._prefix} - {self.id}'
