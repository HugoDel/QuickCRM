#!/usr/bin/env bash
flake8 --version
flake8 ./
flake=$?
echo "flake exit code: ${flake}"
exit ${flake}
