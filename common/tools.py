from datetime import datetime


def get_academic_year(date):
    if date.month < 9:
        return date.year - 1
    return date.year


def get_current_academic_year():
    return get_academic_year(datetime.now())


def format_academic_year(year):
    return f'{year}/{year + 1}'
