from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import ListView, TemplateView, CreateView, DetailView, UpdateView, DeleteView


class AttributeResolver:
    _ATTR_NOT_FOUND = '__ATTR_NOT_FOUND__'

    def _get_attr(self, name):
        v = getattr(self, name, self._ATTR_NOT_FOUND)
        if v == self._ATTR_NOT_FOUND and getattr(self, 'object', False):
            v = getattr(self.object, name, self._ATTR_NOT_FOUND)
        if v == self._ATTR_NOT_FOUND:
            raise Exception('%s not found on %s instance or self.object' % (name, self.__class__.__name__))
        if hasattr(v, '__call__'):
            return v()
        return v


class BaseView(LoginRequiredMixin, TemplateView):
    title = None

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = self.title
        return context


class DefaultListView(LoginRequiredMixin, ListView):
    """
    Standard List View used in most case
    """
    model = None
    title = None
    attr = []

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = self.title or f'{self.model.__name__} List'
        return context


class DefaultTabListView(BaseView):
    """
    Enable a navigation with bootstrap tabs plugin
    Data are fetch with AJAX when the tab is clicked
    nav_item defined the item to display and the link to the a ListView to display => ex:
        nav_item = {'Clients': '/clients', 'Subscribers': '/sub'}
    """
    template_name = 'default_tablist.jinja'
    nav_item = []

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['nav_item'] = self.nav_item
        return context


class DefaultCreateView(LoginRequiredMixin, CreateView):
    template_name = 'default_form.jinja'
    fields = '__all__'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = self.title or f'{self.model.__name__} Create'
        return context


class DefaultDetailsView(LoginRequiredMixin, DetailView):
    model = None
    template_name = 'default_details.jinja'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = self.title or f'{self.model.__name__} Details'
        return context


class DefaultUpdateView(LoginRequiredMixin, UpdateView):
    model = None
    template_name = 'default_form.jinja'
    fields = '__all__'
    title = None
    form_class = None

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = self.title or f'{self.model.__name__} Update'
        return context


class DefaultDeleteView(LoginRequiredMixin, DeleteView):
    model = None
    success_url = None
