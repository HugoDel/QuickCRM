from datetime import datetime

from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.translation import ugettext_lazy as _

from qcauth.models import User


class InvoiceItem(models.Model):
    unit_prince = models.IntegerField(_('Prix unitaire JEH'), default=200)
    unit = models.PositiveSmallIntegerField(_('Nombre de JEH'), default=1)
    invoice = models.ForeignKey('accounting.Invoice',  related_name='items', on_delete=models.CASCADE)
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')


class AdHocCharge(models.Model):

    _prefix = 'CH'
    amount = models.PositiveIntegerField(_('Montant des charges annexes'))
    invoice = models.ForeignKey('accounting.Invoice', verbose_name=_('Facture liée'), on_delete=models.CASCADE)


class AccountingItem(models.Model):
    UNPAID = 0
    NOTE = 10
    CHEQUE = 15
    BANK = 20

    PAYED_BY_TYPE = (
        (UNPAID, _('Non Payée')),
        (NOTE, _('Espèces')),
        (CHEQUE, _('Chèque')),
        (BANK, _('Virement bancaire')),
    )

    _prefix = ''
    tax = models.PositiveSmallIntegerField(_('TVA'), default=0)
    payed_by = models.IntegerField(_('Moyen de payement'), choices=PAYED_BY_TYPE, default=UNPAID)
    payed_ref = models.CharField(_('Ref du moyen de payement'), max_length=150, blank=True, null=True)
    payed_date = models.DateField(_('Date de payement'), auto_now_add=True, null=True, blank=True)
    deleted = models.BooleanField(default=False)

    def get_accounting_name(self):
        return f'{self._prefix}-{self.pk}'

    class Meta:
        abstract = True


class Invoice(AccountingItem):

    _prefix = 'FA'  # Stand for "Invoice" in French


class Receipt(AccountingItem):

    _prefix = 'RE'
    amount = models.IntegerField(_('Montant de l\'adhésion'))


class Subscription(models.Model):

    year = models.PositiveIntegerField(_('Année d\'inscription'))
    adherent = models.ForeignKey('crm.Adherent', related_name='sub',
                                 verbose_name=_('Adherent'), on_delete=models.DO_NOTHING)
    adh_receipt = models.OneToOneField('accounting.Receipt', related_name='sub', verbose_name='Reçu', null=True,
                                       blank=True, on_delete=models.DO_NOTHING)

    class Meta:
        get_latest_by = 'year'

    def validate_sub(self):
        return self.year == datetime.now().year or (datetime.now().year + 1 == self.year and datetime.now().month <= 8)


@receiver(post_save, sender=Subscription)
def create_subscription_document(sender, instance, **kwargs):
    from document.models import Document
    Document.objects.create(
        name=instance.adh_receipt.get_accounting_name(),
        author=User.objects.first(),
        template=Document.DOCS_TEMPLATES[Document.ADH_RECEIPT],
        type=Document.ADH_RECEIPT,
        content_type=ContentType.objects.get_for_model(Receipt),
        object_id=instance.adh_receipt.pk,
    )
