from django import forms
from django.forms import ModelForm

from accounting.models import Receipt


class SetActiveForm(ModelForm, forms.Form):  # Défini le montant de la cotisation

    class Meta:
        model = Receipt
        fields = ['payed_by', 'payed_ref']
