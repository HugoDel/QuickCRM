from django.db import models
from django.core.mail import send_mail
from django.contrib.auth.models import PermissionsMixin
from django.contrib.auth.base_user import AbstractBaseUser
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.base_user import BaseUserManager


class UserManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, email, password, **extra_fields):
        """
        Creates and saves a User with the given email and password.
        """
        if not email:
            raise ValueError('The given email must be set')
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(email, password, **extra_fields)


class User(AbstractBaseUser, PermissionsMixin):
    TITLE_MR = 10
    TITLE_MME = 20
    TITLE_MLLE = 30
    TITLES = (
        (10, _('Mr')),
        (20, _('Mme')),
        (30, _('Mlle')),
    )

    email = models.EmailField(_('Adresse email'), unique=True)
    title = models.IntegerField(_('Titre'), choices=TITLES, null=True)
    first_name = models.CharField(_('Prénom'), max_length=30, blank=True)
    last_name = models.CharField(_('Nom de famille'), max_length=30, blank=True)
    date_created = models.DateTimeField(_('Date d\'ajout'), auto_now_add=True)
    last_updated = models.DateTimeField(_('Dernière modification'), auto_now=True)
    is_active = models.BooleanField(_('Actif'), default=True)
    avatar = models.ImageField(upload_to='avatars/', null=True, blank=True)
    mobile = models.CharField(_('Téléphone portable'), max_length=150, null=True, blank=True)
    phone = models.CharField(_('Téléphone fixe'), max_length=150, null=True, blank=True)
    street = models.TextField(_('Rue'), null=True, blank=True)
    town = models.CharField(_('Ville'), max_length=50, null=True, blank=True)
    zip = models.IntegerField(_('Code Postal'), null=True, blank=True)
    country = models.ForeignKey('crm.Country', verbose_name=_('Pays'),
                                related_name='users', null=True, blank=True, on_delete=models.SET_NULL)

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['first_name']

    class Meta:
        verbose_name = _('User')
        verbose_name_plural = _('Users')

    def get_full_name(self):
        '''
        Returns the first_name plus the last_name, with a space in between.
        '''
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        '''
        Returns the short name for the user.
        '''
        return self.first_name

    def email_user(self, subject, message, from_email=None, **kwargs):
        '''
        Sends an email to this User.
        '''
        send_mail(subject, message, from_email, [self.email], **kwargs)

    def format_address(self):
        if not self.country:
            return None
        return f'{self.street}, {self.town}, {self.zip}, {self.country.abbreviation}'

    def format_phone(self, is_mobile=True):
        # TODO : add international support formating
        tel = self.mobile if is_mobile else self.phone
        return tel
