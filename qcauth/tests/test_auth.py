from django.urls import reverse
from django.test import Client as Cli

from common.tests.test_base import TestBase


class TestAuthentication(TestBase):
    def setUp(self):
        self.login_url = reverse('qcauth:login')
        self.logout_url = reverse('qcauth:logout')
        self.client = Cli()

    def test_anonymous(self):
        pass
