from django.contrib.auth import logout
from django.contrib.auth.views import LoginView
from django.shortcuts import redirect
from django.urls import reverse


class CustomLoginView(LoginView):
    template_name = 'login.jinja'

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx.update({'next': reverse('crm:dashbord')})
        return ctx


login_view = CustomLoginView.as_view()


def logout_view(request):
    logout(request)
    return redirect('qcauth:login')
