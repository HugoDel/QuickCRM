% ==============  Cahier des Charges  ==================

\documentclass[12pt]{article}
\usepackage{capdoc}
\input{mission.tex}
\usepackage{lastpage}
% ===== Variable spécifique au Cahier des charges ==== %
\newcommand{\Titre}{Cahier des Charges\\}
\newcommand{\SsTitre}{\mtitre}
\newcommand{\Date}{\today}
\newcommand{\docref}{CDC\_\reference} % Réference du document

% ==== Calcul automatique avec Lua ==== %
\directlua{dofile("capdoc.lua")}

\begin{document}
\thispagestyle{firstpage}

\betweenspe

Il a été convenu ce qui suit :

%A modifier ----------------------------------------------------------------------------------------------------
Le présent cahier des charges a pour objet de préciser les spécifications de l'étude nécessaire à la réalisation de l'étude \reference~confiée par \csociete~à Cap'EISTI consistant à réaliser una application mobile avec base de données externe pour le compte de la société \csociete, relativement à la convention client CC\reference.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Ce cahier des charges n'est en aucun cas une version finale. Il nécessitera une à plusieurs relectures afin d'affiner les demandes de la société \csociete. C'est uniquement après approbation des parties signataires qu'il sera validé.

\article{1}{Présentation de Cap'EISTI}

Cap'EISTI est la Junior de l'EISTI, l'Ecole Internationale des Sciences du Traitement de l'Information, située à Cergy-Pontoise (95).

Cap'EISTI met à profit les compétences des élèves ingénieurs de l'EISTI pour la réalisation de missions au sein d'entreprises. Nous sommes donc un tremplin entre le monde étudiant et la vie professionnelle.

Nous intervenons dans un large domaine informatique: \'Etudes, formations et développements informatiques et traductions.

\article{2}{Objet du cahier des charges}

Ce cahier des charges a été rédigé par CAP'EISTI pour le compte de la société \csociete. Il correspond à l'avant projet AP\reference~mis en place dans le cadre d'une réalisation d'application mobile avec base de données externe.

\article{3}{Définition du cahier des charges}

%A modifier ----------------------------------------------------------------------------------------------------
\noindent \textbf{\underline{\emph{Avant-propos}}}\\

Ci-dessous, point par point, un descriptif des pages de l'application accompagné de visuels (donnés à titre indicatif). Les besoins pouvant évoluer au cours de l'étude, ce qui suit est sujet à modifications. Ces modifications devront être acceptées par les deux parties. Dans le cas où elles entraineraient une augmentation significative du temps de travail (plus d'une JEH cumulée), Cap'EISTI proposera dans la mesure du possible le remplacement de fonctionnalités par substitution. Dans le cas où la substitution est impossible, un avenant pourra être conclu afin de procéder aux modifications nécessaires du présent cahier des charges.\\

Les phases 1 et 2 de la méthodologie de l'avant projet AP\reference~relatives à l'algorithme de recherche, sont décrites dans l'extrait de brevet, document référencé \emph{Annexe\_01\_13\_WAR}. Il n'y sera donc pas fait mention dans le présent cahier des charges.\\

Tout contenu pouvant être soumis à des droits d'auteurs (images, textes, ...) sera exclusivement fourni par la société \csociete. Cap'EISTI ne pourra être tenu pour responsable de l'utilisation frauduleuse de contenus fournis par la société \csociete.\\

Un document technique référencé Annexe\_02\_13\_WAR explicitant un certain nombre de macros et fonctionnement attendu de l'application a été rédigé par la société \csociete. Cap'EISTI s'appuira dessus pour la réalisation de l'application.\\

\newpage
\noindent \textbf{\underline{\emph{Ouverture de l'application}}}\\

Dans le dock d'un Idevice (Iphone, Ipad) l'application sera représentée par son logo (figure 1).

%\begin{figure}[H]\begin{center}\includegraphics[scale=0.3]{src/logo.png}
%\caption{Logo de l'application Apparel}\end{center}\end{figure}

Au touch (appui tactile) sur le logo, l'application se lance et une page de chargement apparaît. Elle se compose du logo en grand format. La barre verticale du \emph{A} se déploie au fur et à mesure du chargement. L'affichage des pourcentages de chargement suit la barre du \emph{A}. Voir la figure 2 en illustration.\\

%\begin{figure}[H]\begin{center}\fbox{\includegraphics[scale=0.18]{img/1-loading.png}}
%\caption{Page de chargement}\end{center}\end{figure}

\newpage
\noindent \textbf{\underline{\emph{Page d'authentification}}}\\

\`A la fin du chargement, l'utilisateur arrive sur une page d'authentification. Le logo "APPAREL" apparaît en fondu au centre de la page, pour remonter ensuite vers le haut comme présenté dans la figure 3. Afin de pouvoir accèder à la base de données et à l'application, l'utilisateur doit s'identifier avec un login et mot de passe (figure 3).\\

%\begin{figure}[H]\begin{center}\fbox{\includegraphics[scale=0.18]{img/2-login.png}}
%\caption{Page de chargement}\end{center}\end{figure}

\newpage
\noindent \textbf{\underline{\emph{Page d'accueil}}}\\

Une fois identifié, l'utilisateur arrive sur la page d'accueil ou, page principale de l'application. La procédure de recherche d'une tenue se divise en trois grandes étapes : \begin{enumerate}
\item profil : données remplies par l'utilisateur définissant des lois de recherche pour l'algorithme
\item désir : critères subjectifs de souhaits émis par l'utilisateur sous différentes formes
\item tenue : résultats du croisement désir/profils avec l'algorithme de recherche
\end{enumerate}

Un touch sur l'écran, débute le processus de recherche et envoie l'utilisateur vers la page "Article", afin de savoir si l'utilisateur a choisi un vêtement qu'il veut intégrer aux tenues qui lui seront proposées.\\

L'application ne sauvegarde pas les données entrées par l'utilisateur dans une quelconque base de données. En revanche ces données sont temporairement enregistrées dans l'application afin de pouvoir lors d'un retour en arrière faire des modification sans devoir re-entrer tous les champs. \\

%\begin{figure}[H]\begin{center}\fbox{\includegraphics[scale=0.18]{img/3-mainpage.png}}
%\caption{Page de garde - Accueil}\end{center}\end{figure}

\newpage
\noindent \textbf{\underline{\emph{Scan d'articles}}}\\

L'application propose à l'utilisateur d'inclure aux résultats un article qu'il aurait déjà choisis et qui possèderait un code barre. L'application devra alors être capable de scanner ce code barre afin d'inclure le vêtement de l'utilisateur dans les tenues qui lui seront proposées.\\

%\begin{figure}[H]\begin{center}\fbox{\includegraphics[scale=0.18]{img/6-scan.png}}
%\caption{Page article}\end{center}\end{figure}
%\begin{figure}[H]\begin{center}\fbox{\includegraphics[scale=0.18]{img/7-scan.png}}
%\caption{Page scan}\end{center}\end{figure}

\newpage
\noindent \textbf{\underline{\emph{Page profil et désir}}}\\

Ces deux pages sont basiquement des formulaires permettant de fixer un certain nombre de variables qui seront injectées dans l'algorithme de recherche. Se reporter au document détaillant l'algortihme pour plus de détails sur les champs présents.\\

%\begin{figure}[H]\begin{center}\fbox{\includegraphics[scale=0.18]{img/4-1-profil.png}}
%\caption{Page profil - menu}\end{center}\end{figure}
%\begin{figure}[H]\begin{center}\fbox{\includegraphics[scale=0.18]{img/4-2-profil.png}}
%\caption{Page profil - Yeux}\end{center}\end{figure}
%\begin{figure}[H]\begin{center}\fbox{\includegraphics[scale=0.18]{img/4-3-profil.png}}
%\caption{Page profil - Taille}\end{center}\end{figure}
%\begin{figure}[H]\begin{center}\fbox{\includegraphics[scale=0.18]{img/4-4-silfem.png}}
%\caption{Page profil - Silhouette femme}\end{center}\end{figure}
%\begin{figure}[H]\begin{center}\fbox{\includegraphics[scale=0.18]{img/4-5-silhom.png}}
%\caption{Page profil - Silhouette homme}\end{center}\end{figure}

%\begin{figure}[H]\begin{center}\fbox{\includegraphics[scale=0.18]{img/5-desir.png}}
%\caption{Page désir}\end{center}\end{figure}

\newpage
\noindent \textbf{\underline{\emph{Résultat}}}\\

Une fois les étapes précédentes complétées par l'utilisateur, l'algorithme de recherche est lancé. Dans le cas où le résultat ne serait pas instantané le même \emph{A} que dans la page de chargement apparaît. Dès que la première tenue est trouvée, la fenêtre suivante s'affiche. Les autres tenues apparaissent au fur et à mesure..\\

Une fois les résultats obtenus, les vêtements obtenus sont affichés. L'utilisateur peut en cliquant dessus l'ajouter à sa liste de choix.\\

L'utilisateur peut soit passer à l'étape finale (récapitulatif des articles choisis) soit revenir en arrière pour modifier les critères de recherche et relancer l'algorithme.\\

Finalement, une page récapitulative des vêtements choisis apparaît une fois que l'utilisateur a fini de parcourir les résultats proposés par l'algorithme.\\

Dans le cadre d'une utilisation en boutique (Ipad "public"), un bouton "sortie" permet à l'utilisateur se trouvant à l'étape finale d'effacer les données rentrées par l'utilisateur et de revenir à la page de garde.

%\begin{figure}[H]\begin{center}\fbox{\includegraphics[scale=0.18]{img/8-tenue.png}}
%\caption{Page tenues résultat}\end{center}\end{figure}
%\begin{figure}[H]\begin{center}\fbox{\includegraphics[scale=0.18]{img/9-tenue.png}}
%\caption{Page tenue choisie par l'utilisateur}\end{center}\end{figure}

\newpage
\noindent \textbf{\underline{\emph{Ergonomie générale}}}\\

L'appliaction proposera de revenir à l'étape précèdente à tout moment afin de pouvoir modifier les critères de recherche. L'étape en cours devra clairement apparaître à l'utlisateur.\\

Les visuels (figures 1 à 14) établissent la charte graphique. L'application devra s'y conformer le plus possible quant aux proportions des items (titres, étapes, ...), leur emplacement, leur forme. Ces visuels pourront être mis à jour avant le début de la phase 2 de l'avant projet de l'étude. Des modifications sur la forme pourront être apportées en cours de développement dans le respect de l'avant propos ci-dessus.\\ 

\noindent \textbf{\underline{\emph{Crédit}}}\\

Une page de crédit sera mis en place (accessible depuis toutes les pages via un bouton discret), elle contiendra notamment la mention \emph{réalisé par Cap'ESITI, la junior de l'E.I.S.T.I.}, avec un lien vers le site de Cap'EISTI.\\

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\article{4}{Jalons}

%A modifier ----------------------------------------------------------------------------------------------------
Jalons prévisionnel (à titre indicatif):\begin{itemize}
\item Jalon nḞ1 (Algorithme et base de données) : 22/11/2013
\item Jalon nḞ2 (Application) : 17/01/2014
\end{itemize}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\end{document}