import os
from pathlib import Path

from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import SuspiciousOperation
from django.http import HttpResponse, HttpResponseNotFound
from django.shortcuts import redirect
from django.urls import reverse
from django.views.generic.detail import SingleObjectMixin

from common.default_views import BaseView, DefaultDetailsView, DefaultDeleteView
from document.models import Document
from django.utils.translation import ugettext_lazy as _


# ------ Document generation ------
from project.models import Project


class CreateDocument(BaseView, SingleObjectMixin):
    model = Project
    title = _('Créer un document')

    def get(self, *args, **kwargs):
        type = kwargs.get('type')
        if not type:
            raise SuspiciousOperation('Need give doc type identifier')
        template = Document.DOCS_TEMPLATES[type]
        doc = Document.objects.create(
            name='',
            template=template,
            type=type,
            content_type=ContentType.objects.get_for_model(self.get_object()),
            object_id=self.get_object().pk,
            author=self.request.user
        )
        return redirect('doc:document_details', doc.pk)


create_document = CreateDocument.as_view()


class DetailDocumentView(DefaultDetailsView):
    title = _('Document')
    model = Document
    template_name = 'document_details.jinja'

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx.update({
            'modal_title': _('Confirmation'),
            'modal_content': _('Etes-vous sûr de supprimer cette élément'),
            'modal_redirect_url': reverse('doc:delete_document', kwargs={'pk': self.object.pk}),
            'type': Document.DOC_TYPE,
        })
        return ctx


document_details = DetailDocumentView.as_view()


class DeleteDocument(DefaultDeleteView):
    model = Document


delete_document = DeleteDocument.as_view()


def download_doc(request, pk):
    doc = Document.objects.get(pk=pk)
    # TODO : save the file in a directory, and put the path in the DB
    filename = doc.generate()
    # TODO : use FileStorage
    if os.path.exists(filename):
        with open(filename, 'rb') as pdf:
            response = HttpResponse(pdf, content_type='application/pdf')
            response['Content-Disposition'] = f'attachment; filename="{Path(filename).name}"'
            return response
    else:
        return HttpResponseNotFound('The requested pdf was not found in our server.')
