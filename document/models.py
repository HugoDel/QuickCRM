import os

from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.utils.translation import ugettext_lazy as _
from pathlib import Path

from QuickCRM.settings import MEDIA_ROOT
from accounting.models import Invoice
from document.document_builder import ContractBuilder, AdhReceiptBuilder


class Document(models.Model):

    GENERIC = 1
    POST_PROJECT = 10
    BRIEF = 20
    CONTRACT = 30
    INVOICE = 40
    ADH_RECEIPT = 46
    DEPOSIT_INVOICE = 45
    QUOTE = 50

    DOC_TYPE = (
        (GENERIC, _('Generique')),
        (POST_PROJECT, _('Avant-Projet')),
        (BRIEF, _('Cahier des charges')),
        (CONTRACT, _('Contrat')),
        (INVOICE, _('Facture')),
        (DEPOSIT_INVOICE, _('Facture d\'acompte')),
        (QUOTE, _('Devis')),
        (ADH_RECEIPT, _('Reçu d\'adhésion')),
    )

    DOC_ABR = (
        (GENERIC, ''),
        (POST_PROJECT, _('AP')),
        (BRIEF, _('CDC')),
        (CONTRACT, _('CC')),
        (INVOICE, Invoice._prefix),
        (DEPOSIT_INVOICE, _('FAA')),
        (QUOTE, _('DEV')),
        (ADH_RECEIPT, _('RE')),
    )

    DOC_BUILDER = {
        CONTRACT: ContractBuilder,
        ADH_RECEIPT: AdhReceiptBuilder,
    }

    DOCS_TEMPLATES = {
        GENERIC: '',
        POST_PROJECT: '',
        BRIEF: 'CDC.pytexder',
        CONTRACT: 'CC.pytexder',
        INVOICE: '',
        DEPOSIT_INVOICE: '',
        QUOTE: 'Devis.pytexder',
        ADH_RECEIPT: 'subscription_invoice.pytexder',
    }

    name = models.CharField(_('Nom du document'), max_length=150)
    description = models.TextField(_('Description'), null=True, blank=True)
    author = models.ForeignKey('qcauth.User', verbose_name=_('Auteur'), on_delete=models.DO_NOTHING)
    template = models.CharField(_('Modèle de document'), max_length=150)
    create_date = models.DateField(_('Date de création'), auto_now_add=True)
    last_update = models.DateTimeField(_('Dernière modification'), auto_now=True)
    file = models.FileField(verbose_name=_('Nom du fichier'), upload_to='generated/', null=True, blank=True)
    type = models.PositiveSmallIntegerField(_('Type'), choices=DOC_TYPE)
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')

    def generate(self):
        builder = self.DOC_BUILDER[self.type]()
        path = builder.generate(doc=self)
        name = Path(path).name
        new_path = os.path.join(MEDIA_ROOT, name)
        os.rename(path, new_path)
        return new_path


# For Cap'EISTI, document required to be an Adherent
class AdherentDoc(models.Model):
    CV = 10
    ID = 20
    EDU = 30

    SUBS_DOC = (
        (CV, 'CV'),
        (ID, "Carte d'identité"),
        (EDU, 'Carte étudiante'),
    )

    type = models.IntegerField(choices=SUBS_DOC)
    file = models.FileField(verbose_name=_('Nom du fichier'), upload_to='student_doc/', null=False, blank=False)
    data = models.DateField(_('Date de création'), auto_now_add=True)
    adherent = models.ForeignKey('crm.adherent', verbose_name=_('Adhérent'), related_name='reg_docs',
                                 on_delete=models.CASCADE)
