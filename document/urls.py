from django.urls import path

from . import views

app_name = 'doc'
urlpatterns = [
    path('<int:pk>/create_doc/<int:type>/', views.create_document, name='create_document'),
    path('<int:pk>/', views.document_details, name='document_details'),
    path('<int:pk>/delete/', views.delete_document, name='delete_document'),
    path('<int:pk>/download/', views.download_doc, name='download'),
]
