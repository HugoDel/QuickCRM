import os
import locale

import pytexder as pytex

from QuickCRM.settings import PYTEXDER_TEMPLATE_DIR
from accounting.models import Receipt, AccountingItem
from common.tools import format_academic_year
from project.models import Project


# Custom logic for Cap'EISTI
# Tricky way to make images from capdoc.sty working
def sty_latex_helper():
    with open(os.path.join(PYTEXDER_TEMPLATE_DIR, 'capdoc.sty'), 'r') as f:
        d = f.read()
    d = d.replace('@imgpath', os.path.join(PYTEXDER_TEMPLATE_DIR, 'img'))
    with open(os.path.join(PYTEXDER_TEMPLATE_DIR, 'capdoc.sty'), 'w') as f:
        f.write(d)


class AbstractDocumentBuilder:
    ptr = pytex.PyTexder(PYTEXDER_TEMPLATE_DIR)

    def __init__(self):
        # TODO: make this working
        # locale.setlocale(locale.LC_ALL, 'fr_FR.UTF-8')
        pass

    def generate(self, doc, context=None):
        if not context:
            context = self.get_context()
        sty_latex_helper()
        self.ptr.render(doc.template, context, context['name'])
        return os.path.join(PYTEXDER_TEMPLATE_DIR, context['name'] + '.pdf')

    def get_context(self):
        context = {
            'templatedir': PYTEXDER_TEMPLATE_DIR,
            'name': 'document',
        }
        return context


class ContractBuilder(AbstractDocumentBuilder):

    def generate(self, doc, context=None):
        context = self.get_context()
        related_project = Project.objects.get(pk=doc.object_id)
        context.update({
            'doc_title': 'Convention Client',
            'title': related_project.name,
            'ref': related_project.ref_project_formatter(),
            'jeh': 'JEH',
            'delai': 'DELAIS',
            'imgpath': PYTEXDER_TEMPLATE_DIR,
            'name': 'CC_' + related_project.ref_project_formatter(),
        })
        return super().generate(doc, context)


class AdhReceiptBuilder(AbstractDocumentBuilder):

    def generate(self, doc, context=None):
        context = self.get_context()
        related_receipt = Receipt.objects.get(pk=doc.object_id)
        ref = 'RE_' + str(related_receipt.id)
        from document.models import Document
        context.update({
            'payeddate': related_receipt.payed_date.strftime('%d/%m/%Y'),
            'adhname': related_receipt.sub.adherent.user.get_full_name(),
            'name': ref,
            'ref': ref,
            'year': format_academic_year(related_receipt.sub.year),
            'enddate': f'01/09/{related_receipt.sub.year + 1}',
            'doctitle': dict(Document.DOC_TYPE)[doc.type],
            'payedby': dict(AccountingItem.PAYED_BY_TYPE)[related_receipt.payed_by],
            'amount': locale.currency(related_receipt.amount),
            'docdate': doc.create_date.strftime('%d/%m/%Y'),

        })
        return super().generate(doc, context)
