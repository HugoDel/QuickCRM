import $ from 'jquery'
import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'
const feather = require('feather-icons')

function errMsg(msg) {
    const element =
        `<div class="alert alert-warning alert-dismissible fade show msg" role="alert">
            <strong>Oups... </strong> ${msg}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
        </div>`
    $('main').prepend(element)
}

function loadFrame(link, method, target){
    $.ajax({
        url: link,
        data: {},
        method: method,
        beforeSend: function(xhr, settings){
            console.log(`Nav trigger : [${method}] ${link}`)
        },
        success: function(data){
            $(target).html(data)
            feather.replace()
            window.moveTo(0,0)

        },
        error: (xhr, status, err) => {
            errMsg(`Impossible de charger la page, êtes vous connecté ? ${status} : ${err}`)
        },
        dataType: 'html'
    })
}



function loadTabNav() {
    $('.nav-tabs-ajax .nav-link').on('click', function (e) {
        e.preventDefault()
        const url = $(this).attr('data-url')

        if (typeof url !== 'undefined' && url[0] != '#') {
            const pane = $(this), href = this.hash

            // ajax load from data-url
            loadFrame(url, 'GET', href)
            pane.tab('show')
        } else {
            $(this).tab('show')
        }
    })
    $('.nav-tabs-ajax .nav-link:first').trigger('click')
}

function highlightNav(){
    const id_nav = window.location.pathname.split('/')[1]
    $('#'+id_nav).addClass('active')
}


$(function(){
    loadTabNav()
    feather.replace()
    highlightNav()
})