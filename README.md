# QuickCRM (qc)

## Francais

L'association [Cap'EISTI](http://capeisti.fr) développe un CRM pour répondre à ses besoins.
Les principales raisons de créer le notre sont que, en tant que Junior Entreprise,
nous avons besoin de gérer des membres adhérents, des missions avec des documents
spécifiques, un vocabulaire particulier, etc... 

Les principales fonctions disponibles (ou prochainement disponible) sont :

 - Gestion des clients et adhérents
 - Gestion des utilisateurs (type administrateurs, permissions)
 - Création de missions
 - Génération des documents (contrats, factures, ... )

Même si projet est spécifique aux fonctionnement des Juniors, il a pour objectif
de devenir de plus en plus générique dans le futur pour s'adapter au freelance 
dans un premier temps et voir même à tout type d'entreprise. Il sera possible 
d'activer/désactiver certaines fonctions du CRM.

N'hésitez pas à participer au projet ! 

### Comment participer ?

Vous pouvez : 

 - Ouvrir une nouvelle issue
 - Forker le projet et proposer une pull request
 
Mainteneurs : 

- Hugo Delannoy <delannoy_45@msn.com>
- Association Cap'EISTI <contact@capeisti.fr>

### Coniguration du projet sur votre machine

Télécharger l'outil de configuration des packets de MySQL pour installer la
~~version 8~~(cf. issues #6) version 5.7 du serveur. L'outil est disponible [ici](https://dev.mysql.com/get/mysql-apt-config_0.8.10-1_all.deb).
Installer le packet avec `dpkg` : 
```shell
sudo dpkg -i mysql-apt-config_*
```
Lors de l'installation, sélectionner la version 8. Une fois l'installation terminée,
on peut installer le serveur lui même :
```shell
sudo apt update
sudo apt install mysql-community-server python3-dev libmysqlclient-dev
```

Lors de l'installer, veuillez sélectionner l'ancienne méthode d'authentification
pour des raisons de compatibilités.

Créer un fichier `my.cnf` à la racine du projet qui contiendra les informations
de connexion à la base de données :

```
[client]
database = qc
user = root
password = testing
default-character-set = utf8
```

Ou directement depuis le fichier `settings.py` :
```python
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'qc',
        'USER': DB_USER,
        'PASSWORD': DB_PASS,
        'HOST': DB_HOST,
    }
}
```

Tout en ayant avant défini les variables d'environements :
```shell
export db_user=root
export db_pass=<password>
export db_host=localhost
```

Créer ensuite la base de données :
```base
mysql --user="$user" --password="$password" --execute="CREATE DATABASE qc CHARACTER SET utf8;"
```

## English

The association [Cap'EISTI](http://capeisti.fr) develops a CRM to meet its needs.
The main reasons to create ours are that, as a Junior Enterprise,
we need to manage member members, missions with documents
specific vocabulary, etc... 

The main functions available (or soon available) are :

 - Customer and member management
 - User management (administrator type, permissions)
 - Creation of a mission
 - Document generation (contracts, invoices, etc.)

Even if the project is specific to the functioning of Juniors, its objective is to
to become more and more generic in the future to adapt to freelance work 
at first and even to any type of company. It will be possible 
enable/disable certain CRM functions.

Feel free to participate in the project! 

### How to participate?

You can: 

 - Open a new issue
 - Fork the project and propose a pull request

Maintainer:

- Hugo Delannoy <delannoy_45@msn.com>
- Association Cap'EISTI <contact@capeisti.fr>