#!/usr/bin/env bash
mysql -u $db_user -p$db_pass --execute="DROP DATABASE qc; CREATE DATABASE qc CHARACTER SET utf8mb4;"
./manage.py migrate
./manage.py createsuperuser --email test@testing.com --first_name admin && ./manage.py loaddata country.json
