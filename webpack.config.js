var path = require('path');
var webpack = require('webpack');
var BundleTracker = require('webpack-bundle-tracker');
const CleanWebpackPlugin = require('clean-webpack-plugin');

module.exports = {
  context: __dirname,
  entry: {
    main: './assets/js/index.js',
    external: './assets/js/external.js'
  },
  output: {
      path: path.resolve('./QuickCRM/static/dist/'),
      filename: "[name]-[hash].js",
      chunkFilename: "[name]-[hash].bundle.js"
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader']
      }
    ]
  },

  plugins: [
    new BundleTracker({filename: './webpack-stats.json'}),
    new CleanWebpackPlugin(path.resolve('./QuickCRM/static/dist/'))
  ]
}