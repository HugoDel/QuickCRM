from django.forms import ModelForm, ModelChoiceField
from django import forms
from django.utils.translation import ugettext_lazy as _

from crm.models import Company, Adherent
from qcauth.models import User


class ClientForm(ModelForm):
    company = ModelChoiceField(queryset=Company.objects.filter(deleted=False), required=False)

    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'is_active', 'title',
                  'mobile', 'phone', 'street', 'town', 'zip', 'country', 'email']


class AdherentForm(ModelForm, forms.Form):
    eisti_branch = forms.ChoiceField(choices=Adherent.GENIE, label='Genie')
    status = forms.ChoiceField(choices=Adherent.ADHERENT_STATUS, label=_('Status'))
    stu_card = forms.CharField(label=_('N° carte étudiante'), max_length=21)
    n_vital = forms.CharField(label='Num. sécu', max_length=15)
    iban = forms.CharField(label='IBAN', max_length=27, required=False)
    bic = forms.CharField(label='BIC', max_length=11, required=False)
    n_cni = forms.CharField(label='N° CNI', max_length=36)
    skills = forms.CharField(label='Compétences', required=False)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        user = kwargs.get('instance')
        if user and user.role.is_adherent():
            self.fields['stu_card'].initial = user.role.adherent.stu_card
            self.fields['n_vital'].initial = user.role.adherent.n_vital
            self.fields['iban'].initial = user.role.adherent.iban
            self.fields['bic'].initial = user.role.adherent.bic
            self.fields['n_cni'].initial = user.role.adherent.n_cni
            self.fields['eisti_branch'].initial = user.role.adherent.eisti_branch

    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'is_active', 'title',
                  'mobile', 'phone', 'street', 'town', 'zip', 'country', 'email']
