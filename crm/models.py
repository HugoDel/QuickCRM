from django.db import models
from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver
from django.utils.translation import ugettext_lazy as _

from qcauth.models import User


class Role(models.Model):

    TYPE_ADMIN = 'Administrator'
    TYPE_INTERN = 'Intern'
    TYPE_EXTERN = 'Extern'

    _prefix = 'UNKNOWN'
    user = models.OneToOneField(User, related_name='role', on_delete=models.CASCADE)
    role_type = models.CharField(_('Type de role'), max_length=20, editable=False, db_index=True)

    class Meta:
        ordering = ['user__last_updated']
        unique_together = (
            ('user', 'role_type'),
        )

    def display_id(self):
        return f'{self._prefix}-{self.pk}'

    def __str__(self):
        return f'{self.role_type} - {self.user.first_name} {self.user.last_name}'

    def is_adherent(self):
        if self.adherent:
            return True
        return False


# Specific role for Cap'EISTI and the Junior-Entreprise
class Adherent(Role):

    DRAFT = 0
    WAIT_DOC = 10
    ACTIVE = 20
    INACTIVE = 21  # Old one, no more subscription or no money given

    ADHERENT_STATUS = (
        (DRAFT, _('Draft')),
        (WAIT_DOC, _('En attente de doc')),
        (ACTIVE, _('Actif')),
        (INACTIVE, _('Inactif')),
    )

    # Option Maths
    GM = 0
    MF = 1
    IF = 2

    MI = 5
    DS = 6
    SIMU = 7
    FINTEC = 8

    # Option Info
    GI = 20
    SI = 21
    VC = 22
    INEM = 23
    ICC = 24
    SMART = 25

    GSE = 30
    BI = 31
    ERP = 32

    GENIE = (
        (GM, 'GM'),
        (GI, 'GI'),
        (IF, 'Finance'),
        (MI, 'MI'),
        (DS, 'Data Science'),
        (SIMU, 'Simulation informatique'),
        (FINTEC, 'FINTEC'),
        (GI, 'GI'),
        (SI, 'Secu Info'),
        (VC, 'Visual Computing'),
        (INEM, 'INEM'),
        (ICC, 'Cloud Computing'),
        (SMART, 'SMART'),
        (BI, 'BI'),
        (ERP, 'ERP'),
    )

    _prefix = 'AD'
    status = models.IntegerField(_('Status'), choices=ADHERENT_STATUS)
    stu_card = models.CharField('N° de carte étudiante', max_length=21)
    n_vital = models.CharField('Num. sécu', max_length=15)
    n_cni = models.CharField('N° de carte ID', max_length=36)
    iban = models.CharField('IBAN', max_length=27, null=True, blank=True)
    bic = models.CharField('BIC', max_length=11, null=True, blank=True)
    eisti_branch = models.IntegerField('Option', choices=GENIE)
    skills = models.TextField('Compétences', null=True, blank=True)

    def get_adh_branch(self):
        return dict(Adherent.GENIE)[self.eisti_branch]

    def save(self, **kwargs):
        self.role_type = Role.TYPE_INTERN
        super().save(**kwargs)


class Client(Role):
    _prefix = 'TI'  # Tiers person
    company = models.ForeignKey('crm.Company', verbose_name=_('Entreprise'), null=True, blank=True,
                                on_delete=models.SET_NULL)

    def display_id(self):
        return f'{self._prefix}-{self.id}'

    def __str__(self):
        return f'Tiers - {super().__str__()}'

    def save(self, **kwargs):
        self.role_type = self.TYPE_EXTERN
        super().save(**kwargs)


@receiver(post_delete, sender=Role)
def delete_role_user(sender, instance, **kwargs):
    instance.user.delete()


class Country(models.Model):
    name = models.CharField('Nom', max_length=150, unique=True)
    abbreviation = models.CharField('2 Lettres ISO', max_length=5)

    def __str__(self):
        return self.name


class Company(models.Model):
    TYPE_PROVIDER = 10
    TYPE_CLIENT = 20

    COMPANY_TYPES = (
        (10, _('Fournisseur')),
        (20, _('Client')),  # In fact this in more a Business Customer
    )

    PREFIXES = {
        TYPE_CLIENT: 'CL',
        TYPE_PROVIDER: 'FO'  # No already translate so FO = Fournisseur = Provider in French
    }

    name = models.CharField(_('Nom'), max_length=150)
    date_created = models.DateTimeField(_('Date d\'ajout'), auto_now_add=True)
    last_updated = models.DateTimeField(_('Dernière modification'), auto_now=True)
    is_active = models.BooleanField(_('Actif'), default=True)
    street = models.TextField(_('Rue'), null=True, blank=True)
    town = models.CharField(_('Ville'), max_length=50, null=True, blank=True)
    zip = models.IntegerField(_('Code postal'), null=True, blank=True)
    country = models.ForeignKey('crm.Country', verbose_name=_('Pays'),
                                null=True, blank=True, on_delete=models.SET_NULL)
    main_contact = models.OneToOneField('crm.Client', related_name='main_contact', null=False,
                                        blank=False, verbose_name=_('Contact principal'), on_delete=models.CASCADE)
    company_type = models.IntegerField(_('Type d\'entreprise'), choices=COMPANY_TYPES, default=TYPE_CLIENT)
    deleted = models.BooleanField(_('Supprimé'), default=False)

    def format_address(self):
        if not self.country:
            return None
        return f'{self.street}, {self.town}, {self.zip}, {self.country.abbreviation}'

    def display_id(self):
        return f'{self.PREFIXES[self.company_type]}-{self.id}'

    def __str__(self):
        return f'{self.display_id()} - {self.name}'

    def save(self, **kwargs):
        if self.main_contact:
            self.main_contact.company = self
        super().save(**kwargs)
