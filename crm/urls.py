from django.urls import path

from . import views

app_name = 'crm'
urlpatterns = [
    path('', views.index, name='index'),
    path('home', views.home, name='dashbord'),
    path('people', views.people_list, name='people_list'),
    path('people/clients', views.client_list, name='clients_list'),
    path('people/client/add', views.add_client, name='add_client'),
    path('people/client/<int:pk>', views.client_detail, name='client_detail'),
    path('people/client/<int:pk>/update', views.update_client, name='update_client'),
    path('people/client/<int:pk>/delete', views.delete_client, name='delete_client'),
    path('people/companies', views.companies_list, name='companies_list'),
    path('people/company/add', views.add_company, name='add_company'),
    path('people/company/<int:pk>', views.company_detail, name='company_detail'),
    path('people/company/<int:pk>/update', views.update_company, name='update_company'),
    path('people/company/<int:pk>/delete', views.delete_company, name='delete_company'),
    path('people/adherents/', views.adherent_list, name='adherent_list'),
    path('people/adherent/create/', views.add_adherent, name='add_adherent'),
    path('people/adherent/<int:pk>', views.adherent_detail, name='adherent_detail'),
    path('people/adherent/<int:pk>/delete', views.delete_adherent, name='delete_adherent'),
    path('people/adherent/<int:pk>/update', views.update_adherent, name='update_adherent'),
    path('people/adherent/<int:pk>/update/status/<int:status>', views.adh_change_status, name='adh_chg_status'),
]
