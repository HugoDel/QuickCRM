from django.test import Client as Cli
from django.urls import reverse

from common.tests.test_base import TestBase
from crm.models import Client, Company


class TestCRUDClient(TestBase):
    def setUp(self):
        self.client = Cli()

    def test_creation(self):
        r = self.client.get(reverse('crm:clients_list'))
        self.assertContains(r, 'Aucun élement à afficher')
        assert not Client.objects.count()
        r = self.client.get(reverse('crm:add_client'))
        assert r.status_code == 200
        r = self.client.post(reverse('crm:add_client'),
                             data={'first_name': 'Cli', 'last_name': '1', 'country': 1,
                                   'email': 'cli1@test.com', 'title': 10})
        self.assertRedirects(r, reverse('crm:client_detail', kwargs={'pk': Client.objects.get().pk}))
        assert Client.objects.count() == 1
        assert Company.objects.count() == 1

    def test_delete(self):
        pass


class TestAdherent(TestBase):
    def setUp(self):
        pass
