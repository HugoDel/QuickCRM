from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.contenttypes.models import ContentType
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse_lazy, reverse
from django.views import View
from django.views.generic.detail import SingleObjectMixin
from django.views.generic.edit import DeletionMixin

from accounting.forms import SetActiveForm
from accounting.models import Subscription, Receipt
from common.default_views import DefaultListView, DefaultTabListView, DefaultCreateView, DefaultDetailsView, \
    DefaultUpdateView
from common.tools import get_current_academic_year
from crm.forms import ClientForm, AdherentForm
from crm.models import Client, Company, Adherent
from django.utils.translation import ugettext_lazy as _

from document.models import Document
from qcauth.models import User


@login_required
def index(request):
    return redirect('crm:dashbord')


@login_required
def home(request):
    return render(request, 'dashbord.jinja', {'title': 'Dashbord'})


class DeletePeople(View, LoginRequiredMixin, DeletionMixin, SingleObjectMixin):
    model = None
    success_url = reverse_lazy('crm:people_list')

    def delete(self, request, *args, **kwargs):
        object = self.get_object()
        object.delete()
        return redirect(self.success_url)


class PeoplesList(DefaultTabListView):
    model = Client
    title = _('Personnes')
    nav_item = [
        (_('Clients'), reverse_lazy('crm:clients_list')),
        (_('Entreprises'), reverse_lazy('crm:companies_list')),
        (_('Fournisseurs'), '#'),
        (_('Adhérents'), reverse_lazy('crm:adherent_list')),
    ]


people_list = PeoplesList.as_view()


class ClientsList(DefaultListView):
    model = Client
    title = _('Clients')
    template_name = 'client_list.jinja'


client_list = ClientsList.as_view()


class AddClient(DefaultCreateView):
    model = User
    form_class = ClientForm
    title = _('Ajouter un client')
    fields = None

    def get_success_url(self):
        return reverse('crm:client_detail', kwargs={'pk': self.client.pk})

    def form_valid(self, form):
        user = form.save()
        self.client = Client.objects.create(user=user)
        return super().form_valid(form)


add_client = AddClient.as_view()


class ClientDetails(DefaultDetailsView):
    model = Client
    template_name = 'client_details.jinja'
    title = _('Détails Client')

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx.update({
            'modal_title': _('Confirmation'),
            'modal_content': _('Etes-vous sûr de supprimer cette élément'),
            'modal_redirect_url': reverse('crm:delete_client', kwargs={'pk': self.object.pk}),
        })
        return ctx


client_detail = ClientDetails.as_view()


class UpdateClient(DefaultUpdateView):
    title = _('Modifier un client')
    form_class = ClientForm
    model = User
    fields = None

    def get_success_url(self):
        return reverse('crm:client_detail', kwargs={'pk': self.client.pk})

    def form_valid(self, form):
        form.save()
        self.object.role.client.company = form.cleaned_data['company']
        return super().form_valid(form)


update_client = UpdateClient.as_view()


class DeleteClient(DeletePeople):
    model = Client


delete_client = DeleteClient.as_view()


# Company related views

class CompaniesList(DefaultListView):
    model = Company
    title = _('Entreprises')
    template_name = 'company_list.jinja'


companies_list = CompaniesList.as_view()


class AddCompany(DefaultCreateView):
    model = Company
    fields = ['name', 'street', 'town', 'zip', 'country', 'main_contact', 'company_type']
    title = _('Ajouter une Entreprise')

    def get_success_url(self):
        return reverse('crm:company_detail', kwargs={'pk': self.object.pk})


add_company = AddCompany.as_view()


class CompanyDetail(DefaultDetailsView):
    title = _('Détails Entreprise')
    model = Company
    template_name = 'company_details.jinja'

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx.update({
            'modal_title': _('Confirmation'),
            'modal_content': _('Etes-vous sûr de supprimer cette élément'),
            'modal_redirect_url': reverse('crm:delete_company', kwargs={'pk': self.object.pk}),
        })
        return ctx


company_detail = CompanyDetail.as_view()


class UpdateCompany(DefaultUpdateView):
    title = _('Modifier une entreprise')
    model = Company

    def get_success_url(self):
        return reverse('crm:company_detail', kwargs={'pk': self.object.pk})


update_company = UpdateCompany.as_view()


class DeleteCompany(DeletePeople):
    model = Company


delete_company = DeleteCompany.as_view()


# Adherent views


class CreateAdherent(DefaultCreateView):
    form_class = AdherentForm
    model = User
    fields = None
    title = _('Ajouter un adhérent')

    def get_success_url(self):
        return reverse('crm:adherent_detail', kwargs={'pk': self.adh.pk})

    def form_valid(self, form):
        # checked_len_fields = ['iban', 'bic', 'iban']
        user = form.save()
        # The minimum is to check the excepted fields' length
        # TODO
        # for field in checked_len_fields:
        #     if len(form.cleaned_data[field]) != form.fields[field].max_length:
        #         return super().form_invalid(form)
        self.adh = Adherent.objects.create(
            user=user,
            stu_card=form.cleaned_data['stu_card'],
            status=form.cleaned_data['status'],
            n_vital=form.cleaned_data['n_vital'],
            iban=form.cleaned_data['iban'],
            bic=form.cleaned_data['bic'],
            n_cni=form.cleaned_data['n_cni'],
            eisti_branch=form.cleaned_data['eisti_branch'],
        )
        return super().form_valid(form)


add_adherent = CreateAdherent.as_view()


class AdherentList(DefaultListView):
    model = Adherent
    title = _('Adhérent')
    template_name = 'adherent_list.jinja'


adherent_list = AdherentList.as_view()


class AdherentDetail(DefaultDetailsView):
    model = Adherent
    title = _('Details Adhérent')
    template_name = 'adherent_details.jinja'

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx.update({
            'modal_title': _('Confirmation'),
            'modal_content': _('Etes-vous sûr de supprimer cette élément'),
            'modal_redirect_url': reverse('crm:delete_adherent', kwargs={'pk': self.object.pk}),
            'adh_status': dict(Adherent.ADHERENT_STATUS)[self.object.status],
            'status': Adherent.ADHERENT_STATUS,
            'adh_genie': self.object.get_adh_branch(),
        })
        if self.object.sub.exists():
            ctx.update({
                'doc_pk': Document.objects.get(
                    content_type=ContentType.objects.get_for_model(Receipt),
                    object_id=self.object.sub.last().adh_receipt.pk).pk
            })
        return ctx


adherent_detail = AdherentDetail.as_view()


class AdherentUpdate(DefaultUpdateView):
    title = _('Mettre à jour un adhérent')
    model = User
    form_class = AdherentForm
    fields = None

    def get_success_url(self):
        return reverse('crm:adherent_detail', kwargs={'pk': self.object.role.adherent.pk})

    def form_valid(self, form):
        # checked_len_fields = ['iban', 'bic', 'iban']
        form.save()

        # The minimum is to check the excepted fields' length
        # TODO
        # for field in checked_len_fields:
        #     if len(form.cleaned_data[field]) != form.fields[field].max_length:
        #         return super().form_invalid(form)
        Adherent.objects.filter(pk=self.object.role.adherent.pk).update(
            stu_card=form.cleaned_data['stu_card'],
            status=form.cleaned_data['status'],
            n_vital=form.cleaned_data['n_vital'],
            iban=form.cleaned_data['iban'],
            bic=form.cleaned_data['bic']
        )
        return super().form_valid(form)


update_adherent = AdherentUpdate.as_view()


class AdherentDelete(DeletePeople):
    model = Adherent


delete_adherent = AdherentDelete.as_view()


def adh_change_status(request, pk, status):
    form = None
    adh = get_object_or_404(Adherent, pk=pk)
    if not any(s == status for (s, trad) in Adherent.ADHERENT_STATUS):
        return redirect('crm:adherent_detail', pk=pk)
    elif status != Adherent.ACTIVE:
        adh.status = status
        adh.save()
        return redirect('crm:adherent_detail', pk=pk)
    elif request.method == 'POST':
        form = SetActiveForm(request.POST)
        if form.is_valid():
            receipt = form.save()
            Subscription.objects.create(
                year=get_current_academic_year(),
                adherent=Adherent.objects.get(pk=pk),
                adh_receipt=receipt,
            )
            adh.status = status
            adh.save()
            return redirect('crm:adherent_detail', pk=pk)
    else:
        form = SetActiveForm(initial={'adherent': adh})
    return render(request, 'default_form.jinja', {'form': form, 'title': 'Ajouter une adhésion'})
